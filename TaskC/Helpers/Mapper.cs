﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskC.BLL.DTO;
using TaskC.Models;

namespace TaskC.Helpers
{
    public static class Mapper
    {
        public static TourAddDTO Mapping(AddTourModel model)
        {
            return new TourAddDTO
            {
                EndAt = model.EndAt,
                StartAt = model.StartAt,
                Categories = model.Categories,
                Title = model.Title
            };
        }
    }
}
