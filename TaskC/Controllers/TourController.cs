﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TaskC.BLL.Interfaces;
using TaskC.Helpers;
using TaskC.Models;

namespace TaskC.Controllers
{
    public class TourController : Controller
    {
        private readonly ICategoryService _categoryService;
        private readonly ITourService _tourService;

        public TourController(ICategoryService categoryService, 
            ITourService tourService)
        {
            _categoryService = categoryService;
            _tourService = tourService;
        }
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Add()
        {
            var categories = await _categoryService.AllAsync();
            ViewBag.Categories = categories;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Add(AddTourModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.StartAt < model.EndAt && model.Categories.Count > 0)
                {
                    var modelDTO = Mapper.Mapping(model);
                    await _tourService.AddAsync(modelDTO);
                    return RedirectToAction("Get");
                }
                return new JsonResult(model);
            }
            return RedirectToAction("Add");
        }

        public async Task<IActionResult> Get(List<int> filters)
        {
            var categories = await _categoryService.AllAsync();
            ViewBag.Categories = categories;
            var tours = await _tourService.ToListAsync(filters);
            ViewBag.Filters = filters;
            return View("View", tours);
        }
    }
}