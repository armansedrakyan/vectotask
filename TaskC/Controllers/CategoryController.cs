﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TaskC.BLL.Interfaces;
using TaskC.Models;

namespace TaskC.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        public IActionResult Index()
        {
            return View("Add");
        }

        public async Task<IActionResult> Insert(CategoryAddModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _categoryService.InsertAsync(model.Title);
                if (result)
                {
                    return RedirectToAction("Index");
                }
            }
            throw new Exception("Model state is invalid");
        }
    }
}