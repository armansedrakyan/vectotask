﻿using TaskC.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace TaskC.DAL.EF
{
    public class ApplicationDbContext : DbContext
    {

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelbuilder)
        {
            modelbuilder.Entity<CatergoryTour>().HasKey(bc => new { bc.CategoryId, bc.TourId});

        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Tour> Tours { get; set; }
        public DbSet<CatergoryTour> CatergoryTours { get; set; }
    }
}
