﻿using System;
using System.Threading.Tasks;
using TaskC.DAL.Entities;
using TaskC.DAL.Interfaces;
using TaskC.DAL.Repositories;

namespace TaskC.DAL.EF
{
    public class UnitOfWork : IUnitOfWork
    {
        IRepository<CatergoryTour> _categoryTour;
        IRepository<Category> _category;
        IRepository<Tour> _tour;

        private readonly ApplicationDbContext _dbContext;

        public UnitOfWork(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IRepository<Category> Categories
        {
            get
            {
                return _category ??
                    (_category = new BaseRepository<Category>(_dbContext));
            }
        }

        public IRepository<Tour> Tours
        {
            get
            {
                return _tour ??
                    (_tour = new BaseRepository<Tour>(_dbContext));
            }
        }

        public IRepository<CatergoryTour> CategoryTours
        {
            get
            {
                return _categoryTour ??
                    (_categoryTour = new BaseRepository<CatergoryTour>(_dbContext));
            }
        }

        public async Task<bool> CommitAsync()
        {
            if (await _dbContext.SaveChangesAsync() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Rollback()
        {
            throw new NotImplementedException();
        }
    }
}
