﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TaskC.DAL.EF;
using TaskC.DAL.Entities;
using TaskC.DAL.Interfaces;

namespace TaskC.DAL.Repositories
{
    public class CategoryRepository : BaseRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(ApplicationDbContext dbContext)
           : base(dbContext)
        {

        }

      

    }
}
