﻿using TaskC.DAL.Entities;
using TaskC.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using TaskC.DAL.EF;
using System.Threading.Tasks;

namespace TaskC.DAL.Repositories
{
    public class CategoryTourRepository : BaseRepository<CatergoryTour>, ICategoryTourRepository
    {
        public CategoryTourRepository(ApplicationDbContext dbContext)
            :base(dbContext)
        {

        }

        public async Task AddRangeAsync(List<CatergoryTour> model)
        {
            await _dbContext.AddRangeAsync(model);
            await _dbContext.SaveChangesAsync();
        }
    }
}
