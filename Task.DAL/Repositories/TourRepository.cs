﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskC.DAL.EF;
using TaskC.DAL.Entities;
using TaskC.DAL.Interfaces;

namespace TaskC.DAL.Repositories
{
    public class TourRepository : BaseRepository<Tour>, ITourRepository
    {
        public TourRepository(ApplicationDbContext dbContext)
         : base(dbContext)
        {
            // _unitOfWork = unitOfWork;
        }

        /*  public async Task<Tour> InsertAndGetAsync(Tour entity)
          {
              await _dbContext.Tours.AddAsync(entity);
              await _dbContext.SaveChangesAsync();
              return entity;

          }*/

        public async Task<List<Tour>> ToListAsync(List<int> filters=null)
        {
            return await _dbContext.Tours
                .Include(u => u.Categories)
                .ThenInclude(u => u.Category)
                .Where(x => filters.Count > 0 ? x.Categories.Any(x => filters.Contains(x.Category.Id)) : true)
                .ToListAsync();

            
        }
    }
}
