﻿using TaskC.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;

using System.Collections.Generic;
using System.Linq;

using System.Linq.Expressions;
using System.Threading.Tasks;
using TaskC.DAL.EF;

namespace TaskC.DAL.Repositories
{

    public class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        internal ApplicationDbContext _dbContext;
        internal DbSet<TEntity> _dbSet;

        public BaseRepository(ApplicationDbContext context)
        {
            _dbContext = context;
            _dbSet = context.Set<TEntity>();
        }


        public async virtual Task<IEnumerable<TEntity>> GetAsync(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
        {

            IQueryable<TEntity> query = _dbSet;


            if (filter != null)
            {

                query = query.Where(filter);
            }


            if (includeProperties != null)
            {
                foreach (var includeProperty in includeProperties.Split

                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))

                {
                    query = query.Include(includeProperty);
                }
            }

            if (orderBy != null)
            {
                return  await orderBy(query).ToListAsync();
            }
            else
            {

                return await query.ToListAsync();

            }

        }

  

        public async virtual Task InsertAsync(TEntity entity)
        {
            await _dbSet.AddAsync(entity);
        }

        public async virtual Task AddRasngeAsync(List<TEntity> entity)
        {
            await _dbSet.AddRangeAsync(entity);
        }

        public async Task<TEntity> InsertAndGetAsync(TEntity entity)
        {
            await _dbSet.AddAsync(entity);
            await _dbContext.SaveChangesAsync();
            return entity;

        }

        public async Task<List<TEntity>> AllAsync()
        {
            return await _dbSet.ToListAsync();
        }

    }
}
