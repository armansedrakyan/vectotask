﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskC.DAL.Entities
{
    public class Category : BaseEntity
    {
        public string Title { get; set; }
    }
}
