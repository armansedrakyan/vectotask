﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskC.DAL.Entities
{
    public class CatergoryTour
    {
        public int Id { get; set; }
        
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        
        public int TourId { get; set; }
        public Tour Tour { get; set; }

    }
}
