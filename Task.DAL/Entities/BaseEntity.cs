﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskC.DAL.Entities
{
    public class BaseEntity
    {
        public int Id { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
