﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TaskC.DAL.Entities
{
    public class Tour
    {
        public int Id { get; set; }

        public string Title { get; set; }
        public DateTime StartAt { get; set; }
        public DateTime EndtAt { get; set; }

        public ICollection<CatergoryTour> Categories { get; set; }

    }
}
