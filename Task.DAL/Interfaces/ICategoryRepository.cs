﻿using System.Threading.Tasks;
using TaskC.DAL.Entities;

namespace TaskC.DAL.Interfaces
{
    public interface  ICategoryRepository: IRepository<Category>
    {
    }
}
