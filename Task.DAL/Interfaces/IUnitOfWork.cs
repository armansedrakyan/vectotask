﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TaskC.DAL.Entities;

namespace TaskC.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<Category> Categories { get; }
        IRepository<Tour> Tours { get; }
        IRepository<CatergoryTour> CategoryTours{ get; }
        Task<bool> CommitAsync();
    }
}
