﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TaskC.DAL.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class

    {
        Task<List<TEntity>> AllAsync();
        Task<TEntity> InsertAndGetAsync(TEntity entity);
        Task AddRasngeAsync(List<TEntity> entity);

        Task<IEnumerable<TEntity>> GetAsync(

            Expression<Func<TEntity, bool>> filter = null,

            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,

            string includeProperties = "");


        Task InsertAsync(TEntity entity);

    }
}
