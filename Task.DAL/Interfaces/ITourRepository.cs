﻿using TaskC.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace TaskC.DAL.Interfaces
{
    public interface ITourRepository : IRepository<Tour>
    {
        //Task<Tour> InsertAndGetAsync(Tour entity);
        Task<List<Tour>> ToListAsync(List<int> filters);
    }
}
