﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TaskC.BLL.DTO;
using TaskC.DAL.Entities;

namespace TaskC.BLL.Interfaces
{
    public interface ITourService
    {
        Task<bool> AddAsync(TourAddDTO modelDTO);
        Task<List<Tour>> ToListAsync(List<int> filters);
    }
}
