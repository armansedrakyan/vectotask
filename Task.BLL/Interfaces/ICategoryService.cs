﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TaskC.BLL.DTO;

namespace TaskC.BLL.Interfaces
{
    public interface ICategoryService
    {
        Task<bool> InsertAsync(string title);
        Task<List<CategoryDTO>> AllAsync();
    }
}
