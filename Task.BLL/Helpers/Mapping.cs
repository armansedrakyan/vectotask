﻿using System;
using System.Collections.Generic;
using System.Text;
using TaskC.BLL.DTO;
using TaskC.DAL.Entities;

namespace TaskC.BLL.Helpers
{
    public static class Mapping
    {
        public static List<CategoryDTO> Mapp(List<Category> model)
        {
            var list = new List<CategoryDTO>(model.Count);
            foreach (var item in model)
            {
                list.Add(new CategoryDTO
                {
                    Id = item.Id,
                    Title = item.Title
                });
            }
            return list;
        }
    }
}
