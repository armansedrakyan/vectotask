﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using TaskC.BLL.DTO;
using TaskC.DAL.Entities;

namespace TaskC.BLL.Helpers
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<IEnumerable<Category>, List<CategoryDTO>>();
        }
    }
}
