﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskC.BLL.DTO
{
    public class CategoryDTO
    {
        public string Title { get; set; }
        public int Id { get; set; }
    }
}
