﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskC.BLL.DTO
{
    public class TourAddDTO
    {
        public List<int> Categories { get; set; }
        public string Title { get; set; }
        public DateTime StartAt { get; set; }
        public DateTime EndAt { get; set; }
    }
}
