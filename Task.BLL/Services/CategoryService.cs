﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskC.BLL.DTO;
using TaskC.BLL.Interfaces;
using TaskC.DAL.Entities;
using TaskC.DAL.Interfaces;
using TaskC.DAL.Repositories;

namespace TaskC.BLL.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CategoryService(ICategoryRepository categoryRepository,
                IUnitOfWork unitOfWork,
                IMapper mapper)
        {
            _categoryRepository = categoryRepository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<bool> InsertAsync(string title)
        {
          await _unitOfWork.Categories.InsertAsync(new DAL.Entities.Category
            {
                Title = title
            });
            return await _unitOfWork.CommitAsync();
        }

        public async Task<List<CategoryDTO>> AllAsync()
        {
            var result = await _unitOfWork.Categories.AllAsync();
           // throw new Exception(result.FirstOrDefault().Title);
            return TaskC.BLL.Helpers.Mapping.Mapp(result);
        }

    }
}
