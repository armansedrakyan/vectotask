﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TaskC.BLL.DTO;
using TaskC.BLL.Interfaces;
using TaskC.DAL.Entities;
using TaskC.DAL.Interfaces;

namespace TaskC.BLL.Services
{
    public class TourService : ITourService
    {
        private readonly ITourRepository _tourRepository;
        private readonly IUnitOfWork _unitOfWork;

        public TourService(ITourRepository tourRepository,
            IUnitOfWork unitOfWork)
        {
            _tourRepository = tourRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> AddAsync(TourAddDTO modelDTO)
        {
            var categories = await _unitOfWork.Categories.GetAsync(x => modelDTO.Categories.Contains(x.Id));

            var tour = await _unitOfWork.Tours.InsertAndGetAsync(new DAL.Entities.Tour
            {
                EndtAt = modelDTO.EndAt,
                StartAt = modelDTO.StartAt,
                Title = modelDTO.Title
            });

            var insCat = new List<CatergoryTour>();
            foreach (var item in categories)
            {
                insCat.Add(new CatergoryTour
                {
                    CategoryId = item.Id,
                    Tour = tour
                });
            }
            await _unitOfWork.CategoryTours.AddRasngeAsync(insCat);
            return await _unitOfWork.CommitAsync();
        }

        public async Task<List<Tour>> ToListAsync(List<int> filters)
        {
           return await _tourRepository.ToListAsync(filters);
        }
    
    }
}
